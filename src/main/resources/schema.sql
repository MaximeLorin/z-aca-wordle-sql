create table if not exists users
(
	tid varchar(36) PRIMARY KEY,
	email text NOT NULL unique,
	username text NOT NULL
);

create table if not exists games
(
	tid varchar(36) PRIMARY KEY,
	word text NOT NULL,
	max_attempts int NOT NULL,
	user_tid varchar(36) NOT NULL REFERENCES users (tid)
);

create table if not exists rounds
(
	game_tid varchar(36) NOT NULL REFERENCES games (tid),
	word text NOT NULL,
	round_order int,
	CHECK (round_order>=0),
	PRIMARY KEY(word, round_order)
);
