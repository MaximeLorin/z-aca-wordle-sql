package com.zenika.academy.barbajavas.wordle.web.games;

public record GuessRequestDto(String guess, String userTid) {
}
