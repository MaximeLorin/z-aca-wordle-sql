package com.zenika.academy.barbajavas;

import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import com.zenika.academy.barbajavas.wordle.domain.repository.GameRepository;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18nFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

@SpringBootApplication
public class WordleApplication {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Bean
    public I18n i18n(@Value("${wordle.language}") String language) throws Exception {
        return I18nFactory.getI18n(language);
    }
    
    @Bean
    public Scanner scanner() {
        return new Scanner(System.in);
    }
    
    public static void main(String[] args) throws Exception {
        SpringApplication.run(WordleApplication.class);
    }
//    @PostConstruct
//    public void addUser() {
//            User u = jdbcTemplate.queryForObject("select * from users where tid=?", new UserRowMapper(),"16e638f0-3806-4f29-8f52-0a918b3e4cfe");
//
////            return jdbcTemplate.update(
////                    "INSERT INTO users VALUES (?, ?, ?)", "16e638f0-3806-4f29-8f52-0a918b3e4cfe", "Bill", "Gates");
//    }
//    private class UserRowMapper implements RowMapper<User>{
//        @Override
//        public User mapRow(ResultSet rs, int rowNum) throws SQLException{
//            //String userTid =rs.getString(tid);
//            return null;
//        }
//    }
}
